#!/usr/bin/env python3

"""The main launcher. Run this module to start all bots via webhooks.

Based on:
https://github.com/eternnoir/pyTelegramBotAPI/blob/master/examples/webhook_examples/webhook_aiohttp_echo_bot.py
"""

import os
import sys
import ssl
import telebot
from aiohttp import web
from importlib import import_module
from settings import *


def read_mappings() -> dict:
    """Scans folders and finds bots.
    :returns: {'<token>': <specific TeleBot instance>, ...}
    """

    dirs = (s for s in os.listdir('.') if (s[0] != '.'
                                           and os.path.isdir(s)
                                           and os.path.isfile(os.path.join(s, "settings.py"))))
    mappings = {}
    for d in dirs:
        sys.path.append(d)
        bot = import_module(d).bot
        settings = import_module("%s.%s" % (d, "settings"))
        mappings[settings.TOKEN] = bot
    return mappings

async def handler(request):
    """Processes webhook calls."""

    token = request.match_info.get_info()['path'][1:-1]
    if token in mappings:
        request_body_dict = await request.json()
        update = telebot.types.Update.de_json(request_body_dict)
        mappings[token].process_new_updates([update])
        return web.Response()
    else:
        return web.Response(status=403)


mappings = read_mappings()
app = web.Application()

# Sets webhooks for all bots.
for token, bot in mappings.items():
    app.router.add_post('/%s/' % token, handler)
    # Removes the previous webhook. According to the example, the setting fails sometimes if don't do it.
    bot.remove_webhook()
    with open(SSL_CERT, 'r') as f:
        bot.set_webhook(url="https://%s:%i/%s/" % (HOST, PORT, token), certificate=f)

context = ssl.SSLContext(ssl.PROTOCOL_TLSv1_2)
context.load_cert_chain(SSL_CERT, SSL_PRIV)


if __name__ == '__main__':
    # Starts aiohttp server
    web.run_app(
        app,
        host=LISTEN_TO,
        port=PORT,
        ssl_context=context,
    )
