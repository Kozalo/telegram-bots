"""This module provides you a way to schedule some actions for later execution.

It uses a special singleton class (TimeLine), which is executing in a separate thread and checking if there are any
actions to be called. All bots should use this class for such pending tasks for performance reasons.
"""

from threading import Thread, Lock
from telebot import TeleBot, logger
from telebot.apihelper import ApiException
import time


class Action:
    """A base action that takes a callable and arguments for it and executes it after a specified number of seconds."""

    def __init__(self, delay: int, action: callable, *args, **kwargs):
        self._creation_time = time.time()
        self._delay = delay

        self._action = action
        self._args = args
        self._kwargs = kwargs

    @property
    def time_has_come(self) -> bool:
        """:returns: True if the lifetime of a message is over."""
        return time.time() - self._creation_time > self._delay

    def run(self):
        """Calls the action and returns its result."""
        return self._action(*self._args, **self._kwargs)


class DoomedMessage(Action):
    """To schedule deletion of some message after a specified number of seconds, wrap it in an instance of this class
    and append it to the timeline."""

    def __init__(self, bot: TeleBot, message, lifetime: int):
        def deletion_func():
            try:
                self._bot.delete_message(self.chat_id, self.id)
            except ApiException:
                pass

        super().__init__(lifetime, deletion_func)
        self._bot = bot
        self._id = message.message_id
        self._chat_id = message.chat.id

    @property
    def id(self) -> int:
        return self._id

    @property
    def chat_id(self) -> int:
        return self._chat_id


class Singleton(type):
    """A metaclass."""

    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super().__call__(*args, **kwargs)
        return cls._instances[cls]


class TimeLine(metaclass=Singleton):
    """A storage for all actions and a main working class. It's responsible for creating and destruction of a separate
    working thread. You don't need to worry about that since TimeLine is smart enough to stop the thread when there are
    no actions in the queue, and recreate it again when it will be needed.
    """

    def __init__(self, idle_time=1):
        self.idle_time = idle_time
        self._queue = []
        self._working_thread = None
        self._lock = Lock()
        self._stop_flag = False
        self._running = False

    def append(self, action: Action):
        """Adds a given action to the queue.
        If the working thread isn't running yet, a new one will be created automatically.
        """

        with self._lock:
            self._queue.append(action)

        if not self._running:
            self._working_thread = Thread(target=self.__run)
            self._working_thread.start()

    def stop(self):
        """Use it to stop the working thread if something goes wrong."""
        self._stop_flag = True

    @property
    def running(self) -> bool:
        """Return True is the working thread is running."""
        return self._running

    def __run(self):
        """The working method that loops over actions and calls them, if their time has come.
        If there are no other messages in the queue, the thread finishes."""

        self._stop_flag = False
        self._running = True

        while not self._stop_flag and self._queue:
            for action in self._queue:
                if action.time_has_come:
                    try:
                        action.run()
                    except Exception as err:
                        logger.exception(err)
                    with self._lock:
                        self._queue.remove(action)
            time.sleep(self.idle_time)

        self._running = False
