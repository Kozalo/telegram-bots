from telebot.types import Message, InlineQuery
from telebot import logger


DEFAULT_LANG = '_'


class LocalizationFactory:
    def __init__(self, dct: dict):
        if DEFAULT_LANG in dct.keys():
            self._dct = dct
        elif DEFAULT_LANG in next(iter(dct.values())):
            self._dct = {}
            for key, translations in dct.items():
                for lang, text in translations.items():
                    if lang not in self._dct:
                        self._dct[lang] = {}
                    self._dct[lang][key] = text
        else:
            raise ValueError("Invalid language")

        self._spare_dct = SpecificLanguageDictionary(DEFAULT_LANG, self._dct[DEFAULT_LANG],
                                                     LanguageDictionary())

    def get_lang(self, obj):
        if isinstance(obj, Message) or isinstance(obj, InlineQuery):
            obj = obj.from_user
        lang_code = obj.language_code

        if lang_code:
            if lang_code in self._dct:
                return SpecificLanguageDictionary(lang_code, self._dct[lang_code], self._spare_dct)
            elif lang_code[:2] in self._dct:
                return SpecificLanguageDictionary(lang_code, self._dct[lang_code[:2]], self._spare_dct)
        return self._spare_dct

    def get_phrase(self, msg_or_usr, key: str):
        lang_dct = self.get_lang(msg_or_usr)
        return lang_dct[key]


class LanguageDictionary:
    def __init__(self, name=DEFAULT_LANG):
        self._name = name

    def __contains__(self, item):
        return True

    def __getitem__(self, item: str) -> str:
        logger.warning("Localized phrase not found at all: %s" % item)
        return item

    @property
    def name(self) -> str:
        return self._name


class SpecificLanguageDictionary(LanguageDictionary):
    def __init__(self, name: str, dct: dict, spare_dct: LanguageDictionary):
        super().__init__(name)
        self._dct = dct
        self._spare_dict = spare_dct

    def __getitem__(self, item: str) -> str:
        if item in self._dct:
            return self._dct[item]
        elif item in self._spare_dict:
            logger.warning("Localized phrase for %s not found, used for %s." % (self.name, self._spare_dict.name))
            return self._spare_dict[item]
        else:
            return super()[item]
