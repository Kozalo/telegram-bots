"""This module is intended to provide some useful builders for standard telebot types."""

from .telebot.types import *


class InlineQueryResultsBuilder:
    """This class gives you a way to create a list of **InlineQueryResultArticle**s with incremental numeric ids
    starting from 1.
    Use `new_*()` methods to add items into an internal collection. Call `build_list()` to get its copy then.
    """

    def __init__(self):
        self._items = []
        self._counter = 0

    def _new_object(self, obj) -> "InlineQueryResultsBuilder":
        """Use this method to add a new item into the collection."""

        self._counter += 1
        self._items.append(obj)
        return self

    @property
    def empty(self) -> bool:
        """:returns *True* if the length of the collection is equal to zero."""
        return len(self._items) == 0

    def build_list(self) -> list:
        """:returns: a copy of the collection."""
        return self._items.copy()

    def new_article(self, title: str, **kwargs) -> "InlineQueryResultsBuilder":
        """Creates a new article with specified parameters and appends it to the list."""

        obj = InlineQueryResultArticle(self._counter, title, **kwargs)
        return self._new_object(obj)

    def new_photo(self, photo_id: str, **kwargs) -> "InlineQueryResultsBuilder":
        """Creates a new photo with specified parameters and appends it to the list."""

        obj = InlineQueryResultCachedPhoto(self._counter, photo_id, **kwargs)
        return self._new_object(obj)

    def new_gif(self, gif_id: str, **kwargs) -> "InlineQueryResultsBuilder":
        """Creates a new gif with specified parameters and appends it to the list."""

        obj = InlineQueryResultCachedGif(self._counter, gif_id, **kwargs)
        return self._new_object(obj)

    def new_audio(self, audio_id: str) -> "InlineQueryResultsBuilder":
        """Creates a new audio with specified parameters and appends it to the list."""

        obj = InlineQueryResultCachedAudio(self._counter, audio_id)
        return self._new_object(obj)

    def new_voice(self, voice_id: str, title: str, **kwargs) -> "InlineQueryResultsBuilder":
        """Creates a new voice with specified parameters and appends it to the list."""

        obj = InlineQueryResultCachedVoice(self._counter, voice_id, title, **kwargs)
        return self._new_object(obj)

    def new_sticker(self, sticker_id: str, **kwargs):
        obj = InlineQueryResultCachedSticker(self._counter, sticker_id)
        return self._new_object(obj)
