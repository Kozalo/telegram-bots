"""Various decorators that may be useful for bots."""

from telebot.types import Message
from typing import Optional, Any


def ignore_if_forwarded(func) -> callable:
    """Decorator that executes a wrapped function and returns its result if the message is not a forwarded one.
    None will be returned otherwise."""

    def decorator(message: Message, *args, **kwargs) -> Optional[Any]:
        if not message.forward_date:
            return func(message, *args, **kwargs)
    return decorator
