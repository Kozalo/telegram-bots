import telebot
import time
import re

from typing import Any, Union, Optional
from telebot import logger
from telebot.apihelper import ApiException
from telebot.util import extract_arguments

from ..helpers import is_group_chat
from ..scheduler import TimeLine, DoomedMessage
from .types import Message
from .extensions import TimeoutConstraintGroup, UserList


class TeleBot(telebot.TeleBot):
    def __init__(self, token, threaded=True, skip_pending=False, limitation_period=604800):
        super().__init__(token, threaded, skip_pending)
        self.limitation_period = limitation_period
        self._privileged_user_ids = []
        self._timeouts = {}
        self._name = self.get_me().username

    @property
    def name(self):
        return self._name

    def _test_filter(self, filter, filter_value, message):
        def ensure_list(obj):
            return obj if type(obj) in (list, tuple) else (obj,)

        def regexp_processor(expr, flags=0):
            def func(msg):
                if msg.content_type != "text":
                    return False

                match = re.search(expr, msg.text, flags)
                if match:
                    msg.match = match
                    return True
                else:
                    return False
            return func

        def regexp_list_processor(expressions):
            def func(msg):
                if msg.content_type != "text":
                    return False

                for expr in expressions:
                    match = re.search(expr, msg.text, re.IGNORECASE)
                    if match:
                        msg.match = match
                        return True
                return False
            return func

        test_cases = {
            'content_types': lambda msg: msg.content_type in ensure_list(filter_value),
            'texts': lambda msg: msg.content_type == "text" and any((msg.text.lower() == x.lower()
                                                                     for x in ensure_list(filter_value))),
            'case_sensitive_texts': lambda msg: msg.content_type == "text" and msg.text in ensure_list(filter_value),
            'regexp': regexp_processor(filter_value, re.IGNORECASE),
            'case_sensitive_regexp': regexp_processor(filter_value),
            'phrases': regexp_list_processor((r"\b%s\b" % x for x in ensure_list(filter_value))),
            'commands': lambda msg: msg.content_type == "text"
                                                        and self.extract_command(msg.text) in ensure_list(filter_value),
            'func': lambda msg: filter_value(msg)
        }

        return test_cases.get(filter, lambda msg: False)(message)

    def _notify_command_handlers(self, handlers, new_messages):
        now = time.time()
        for message in new_messages:
            if hasattr(message, "date") and now - message.date > self.limitation_period:
                continue

            message.match = None    # Make `telebot.types.Message` compatible to `.types.Message`
            for message_handler in handlers:
                if self._test_message_handler(message_handler, message):
                    self._exec_task(message_handler['function'], message)
                    break

    def send_message(self, chat_id, text, disable_web_page_preview=None, reply_to_message_id=None, reply_markup=None,
                     parse_mode=None, disable_notification=None):
        return super().send_message(chat_id, str(text), disable_web_page_preview, reply_to_message_id, reply_markup,
                                    parse_mode, disable_notification)

    def answer_to(self, message, text, delete_after=None, **kwargs):
        msg_id = self.send_message(message.chat.id, text, **kwargs)
        if delete_after:
            TimeLine().append(DoomedMessage(self, msg_id, delete_after))
        return msg_id

    def reply_to(self, message, text, delete_after=None, **kwargs):
        msg_id = super().reply_to(message, text, **kwargs)
        if delete_after:
            TimeLine().append(DoomedMessage(self, msg_id, delete_after))
        return msg_id

    def change_keyboard(self, query, markup, *args, **kwargs):
        self.edit_message_reply_markup(query.message.chat.id,
                                       query.message.message_id,
                                       reply_markup=markup,
                                       *args, **kwargs)

    def grant_privilege(self, *ids):
        self._privileged_user_ids += ids

    def has_privilege(self, user_id: int, chat_id: int = None):
        lst = self._get_admin_ids(chat_id) if chat_id else self._privileged_user_ids
        return user_id in lst

    def answer(self, *args, delete_after=None, **kwargs):
        """Decorator. Sends the text returned by the wrapped function as a message."""
        def generator(func):
            def decorator(message, *d_args, **d_kwargs):
                text = func(message, *d_args, **d_kwargs)
                if text:
                    self.answer_to(message, text, delete_after, **kwargs)
                return text
            return decorator

        if not kwargs and delete_after is None and len(args) == 1 and callable(args[0]):
            return generator(args[0])
        else:
            return generator

    def reply(self, *args, delete_after=None, **kwargs):
        """Decorator. Replies with the text returned by the wrapped function."""
        def generator(func):
            def decorator(message, *d_args, **d_kwargs):
                text = func(message, *d_args, **d_kwargs)
                if text:
                    self.reply_to(message, text, delete_after, **kwargs)
                return text
            return decorator

        if not kwargs and delete_after is None and len(args) == 1 and callable(args[0]):
            return generator(args[0])
        else:
            return generator

    def reply_if_group(self, *args, delete_after=None, **kwargs):
        """
        Decorator. If the chat is a group chat, replies with the text returned by the wrapped function. Otherwise,
        sends the text as a usual message.
        """
        def generator(func):
            def decorator(message, *d_args, **d_kwargs):
                text = func(message, *d_args, **d_kwargs)
                if text:
                    msg_id = message.message_id if "group" in message.chat.type else None
                    sent_msg_id = self.send_message(message.chat.id, text, reply_to_message_id=msg_id, **kwargs)
                    if delete_after:
                        TimeLine().append(DoomedMessage(self, sent_msg_id, delete_after))
                return text
            return decorator

        if not kwargs and delete_after is None and len(args) == 1 and callable(args[0]):
            return generator(args[0])
        else:
            return generator

    def only_for_admins(self,
                        forbidden_message: str = "Forbidden! Only administrators are allowed to use this command.",
                        forbidden_callback: callable = None,
                        as_reply: bool = True):
        """
        Decorator. Calls the wrapped function only if the sender is either an administrator or privileged user (they are
        added by `grant_privilege` function). If it's not so, a `forbidden_callback` function will be called if
        specified (otherwise, the `forbidden_message` message will be sent).
        """
        def generator(func):
            def decorator(message, *d_args, **d_kwargs):
                if message.from_user.id in self._get_admin_ids(message.chat.id):
                    return func(message, *d_args, **d_kwargs)
                elif callable(forbidden_callback):
                    return forbidden_callback(message, *d_args, **d_kwargs)
                elif as_reply:
                    self.reply_to(message, forbidden_message)
                else:
                    self.answer_to(message, forbidden_message)
            return decorator
        return generator

    def user_required(self,
                      no_user_message: str = "Reply to the user's message!",
                      no_user_callback: callable = None,
                      as_reply: bool = True):
        """
        Decorator. Request information about the user (the user which name was specified (but not username!) OR
        the author of the replied message OR the user which ID was specified) and pass it into the wrapped function
        as the first argument. If there is no any data to determine the user, a `no_user_callback` function will be
        called if specified (otherwise, the `no_user_messsage` message will be sent).
        """
        def parse_user(message):
            user = None
            for entity in (x for x in message.entities if x.type == "text_mention"):
                user = entity.user
                break
            if user is None:
                if message.reply_to_message:
                    user = message.reply_to_message.from_user
                else:
                    user_tag = extract_arguments(message.text)
                    if user_tag:
                        try:
                            user = self.get_chat_member(message.chat.id, user_tag).user
                        except ApiException as err:
                            logger.exception(err)
            return user

        def generator(func):
            def decorator(message, *d_args, **d_kwargs):
                user = parse_user(message)
                if user:
                    return func(user, message, *d_args, **d_kwargs)
                elif callable(no_user_callback):
                    return no_user_callback(message, *d_args, **d_kwargs)
                elif as_reply:
                    self.reply_to(message, no_user_message)
                else:
                    self.answer_to(message, no_user_message)
            return decorator

        return generator

    def timeout_constraint(self, group: TimeoutConstraintGroup = None,
                           timeout: Union[int, callable, None] = None,
                           for_admins_too: bool = False,
                           for_all: bool = False,
                           not_passed_callback: Optional[callable] = None) -> callable:
        """
        Decorator. Allows you to restrict the access to some command based on `timeout` in seconds (can be calculated at
        runtime if passed as a callable). You may want to restrict different groups of commands independently. Use
        different instances of the `TimeoutConstraintGroup` class instead of the `timeout` argument for that. You can
        restrict admins too if you want. Or use one common timeout for all users in the chat.

        If the constraint check was not passed, a `not_passed_callback` function is called if specified.
        """
        assert group or timeout
        if not group:
            group = TimeoutConstraintGroup("__DEFAULT__", timeout, for_admins_too, for_all)

        def generator(func: callable) -> callable:
            def decorator(message: Message, *d_args, **d_kwargs) -> Any:
                chat_id = message.chat.id
                user_id = message.from_user.id if not group.for_all else 0
                timeout = group.resolve_timeout(message)

                if is_group_chat(message) and (group.for_admins_too or not self.has_privilege(user_id, chat_id)):
                    if chat_id not in self._timeouts:
                        self._timeouts[chat_id] = {}
                    if group.name not in self._timeouts[chat_id]:
                        self._timeouts[chat_id][group.name] = {}
                    if user_id not in self._timeouts[chat_id][group.name]:
                        self._timeouts[chat_id][group.name][user_id] = 0

                    if time.time() - self._timeouts[chat_id][group.name][user_id] < timeout:
                        return not_passed_callback(message) if not_passed_callback else None
                    self._timeouts[chat_id][group.name][user_id] = time.time()

                return func(message, *d_args, **d_kwargs)

            return decorator
        return generator

    def reset_timeout(self, chat_id, *user_ids):
        for user_id in user_ids:
            for group in self._timeouts[chat_id].values():
                if user_id in group:
                    group[user_id] = 0

    def user_constraint(self, exclude: UserList, include_admins_anyway: bool = False,
                        not_passed_callback: Optional[callable] = None) -> callable:
        """
        Decorator. Allows you to restrict some users of using some commands. You can restrict admins too if you want.
        If the constraint check was not passed, a `not_passed_callback` function is called if specified.
        """
        def generator(func: callable) -> callable:
            def decorator(message: Message, *d_args, **d_kwargs) -> Any:
                chat_id = message.chat.id
                user_id = message.from_user.id

                if is_group_chat(message) and not (include_admins_anyway and self.has_privilege(user_id, chat_id)):
                    if user_id in exclude.resolve_list(message):
                        return not_passed_callback(message) if not_passed_callback else None

                return func(message, *d_args, **d_kwargs)

            return decorator
        return generator

    def extract_command(self, text):
        """
        Extracts the command from `text` (minus the '/') if `text` is a command (see is_command).
        If `text` is not a command, this function returns None.

        Examples:
        extract_command('/help'): 'help'
        extract_command('/help@BotName'): 'help'
        extract_command('/help@AnotherBotName'): None
        extract_command('/search black eyed peas'): 'search'
        extract_command('Good day to you'): None

        :param text: String to extract the command from
        :return: the command if `text` is a command (according to is_command), else None.
        """

        if not text.startswith('/'):
            return None

        command = text.split()[0].split('@')
        if len(command) > 1 and command[1] != self.name:
            return None
        return command[0][1:]

    def _get_admin_ids(self, chat_id):
        admin_ids = list(self._privileged_user_ids)
        try:
            admins = self.get_chat_administrators(chat_id)
        except ApiException as err:
            logger.exception(err)
            return []
        admin_ids += [a.user.id for a in admins]
        return admin_ids
