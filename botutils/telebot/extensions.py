from typing import Any, Union
from telebot.types import Message


class TimeoutConstraintGroup:
    def __init__(self, name: Any, timeout: Union[int, callable],
                 for_admins_too: bool = False,
                 for_all: bool = False) -> None:
        self._name = name
        self._timeout = timeout
        self._for_admins_too = for_admins_too
        self._for_all = for_all

    @property
    def name(self) -> Any:
        return self._name

    @property
    def timeout(self) -> Union[int, callable]:
        return self._timeout

    @timeout.setter
    def timeout(self, value: Union[int, callable]) -> None:
        self.timeout = value

    @property
    def for_admins_too(self) -> bool:
        return self._for_admins_too

    @property
    def for_all(self) -> bool:
        return self._for_all

    def resolve_timeout(self, message: Message) -> int:
        return self._timeout(message) if callable(self._timeout) else self._timeout


class UserList:
    def __init__(self, obj: Union[list, tuple, callable]) -> None:
        self._obj = obj

    @property
    def value(self) -> Union[list, tuple, callable]:
        return self._obj

    def resolve_list(self, message: Message) -> Union[list, tuple]:
        return self._obj(message) if callable(self._obj) else self._obj
