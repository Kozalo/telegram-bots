from telebot import types


class Message(types.Message):
    def __init__(self, message_id, from_user, date, chat, content_type, options):
        self.new_chat_members = None
        self.match = None
        super().__init__(message_id, from_user, date, chat, content_type, options)


class ReplyKeyboardMarkup(types.ReplyKeyboardMarkup):
    def __init__(self, *button_names, resize_keyboard=True, one_time_keyboard=None, selective=None, row_width=3):
        super().__init__(resize_keyboard, one_time_keyboard, selective, row_width)
        for button_name in button_names:
            self.add(types.KeyboardButton(button_name))


class InlineKeyboardMarkup(types.InlineKeyboardMarkup):
    def __init__(self, *buttons, row_width=3):
        super().__init__(row_width)
        self.add(*buttons)


class InlineKeyboardButton(types.InlineKeyboardButton):
    def __init__(self, text, url=None, callback_data=None, switch_inline_query=None,
                 switch_inline_query_current_chat=None, callback_game=None, pay=None):
        super().__init__(str(text), url, str(callback_data), switch_inline_query, switch_inline_query_current_chat,
                         callback_game, pay)


class InlineQueryResultArticle(types.InlineQueryResultArticle):
    def __init__(self, id, title, text=None, input_message_content=None, reply_markup=None, url=None, hide_url=None,
                 description=None, thumb_url=None, thumb_width=None, thumb_height=None, parse_mode="Markdown"):
        self.text = text
        if not input_message_content:
            input_message_content = types.InputTextMessageContent(str(text), parse_mode=parse_mode)
        super().__init__(str(id), title, input_message_content, reply_markup, url, hide_url, description, thumb_url,
                         thumb_width, thumb_height)


class InlineQueryResultCachedPhoto(types.InlineQueryResultCachedPhoto):
    def __init__(self, id, photo_file_id, title=None, description=None, caption=None, reply_markup=None,
                 input_message_content=None):
        super().__init__(str(id), photo_file_id, title, description, caption, reply_markup, input_message_content)


class InlineQueryResultCachedGif(types.InlineQueryResultCachedGif):
    def __init__(self, id, gif_file_id, title=None, description=None, caption=None, reply_markup=None,
                 input_message_content=None):
        super().__init__(str(id), gif_file_id, title, description, caption, reply_markup, input_message_content)


class InlineQueryResultCachedAudio(types.InlineQueryResultCachedAudio):
    def __init__(self, id, audio_file_id, caption=None, reply_markup=None, input_message_content=None):
        super().__init__(str(id), audio_file_id, caption, reply_markup, input_message_content)


class InlineQueryResultCachedVoice(types.InlineQueryResultCachedVoice):
    def __init__(self, id, voice_file_id, title, caption=None, reply_markup=None, input_message_content=None):
        super().__init__(str(id), voice_file_id, title, caption, reply_markup, input_message_content)


class InlineQueryResultCachedSticker(types.InlineQueryResultCachedSticker):
    def __init__(self, id, sticker_file_id, reply_markup=None, input_message_content=None):
        super().__init__(str(id), sticker_file_id, reply_markup, input_message_content)
