"""This module contains different useful functions that you may want to use."""

from telebot.types import User, Message


def get_username(user) -> str:
    """:returns: either the @username (if it's set) or the first and last names of a user.
    :type user: User or dict
    """

    if isinstance(user, User):
        username, first_name, last_name = user.username, user.first_name, user.last_name
    elif type(user) is dict:
        safe_get = get_safe_get_closure(user)
        username, first_name, last_name = safe_get("username"), user['first_name'], safe_get("last_name")
    else:
        raise TypeError("The user parameter must be either a User or dict!")

    if username:
        return '@' + username

    username = first_name
    if last_name:
        username += ' ' + last_name
    return username


def get_safe_get_closure(dct: dict, default=None) -> callable:
    """Use this function to get a closure function that wraps a dict and returns the default value instead of raising
    a KeyError exception."""

    def closure(key):
        return dct[key] if key in dct else default
    return closure


def is_group_chat(message: Message) -> bool:
    """:returns: True if the message was sent from a group or super group."""
    return "group" in message.chat.type


def is_private_chat(message: Message) -> bool:
    """:returns: True if the message was sent from a private chat."""
    return message.chat.type == "private"


def escape_html(text: str) -> str:
    """Replaces all angle brackets with HTML entities."""
    return text.replace('<', '&lt;').replace('>', '&gt;')
