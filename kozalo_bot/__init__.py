#!/usr/bin/env python3

import re
import random
from functools import partial
from typing import Any, Union, Iterable, Optional

from cachetools import cached, TTLCache

from botutils.telebot import TeleBot
from botutils.telebot.types import Message
from botutils.telebot.extensions import TimeoutConstraintGroup, UserList
from botutils.scheduler import TimeLine, DoomedMessage
from botutils.helpers import get_username, is_private_chat, is_group_chat, escape_html
from botutils.decorators import ignore_if_forwarded
from botutils.builders import InlineQueryResultsBuilder
from telebot.apihelper import ApiException
from telebot.types import User, InlineQuery
from telebot.util import extract_arguments

from kozalo_bot.settings import *
from storage import *
from informator import get_info_on


# INITIALIZATION

bot = TeleBot(TOKEN)
bot.grant_privilege(68761694)   # @kozalo

restore_state()


# UTILITY FUNCTIONS

def get_random_item(lst: Union[list, tuple]) -> Any:
    if len(lst) == 0:
        return None
    i = random.randrange(len(lst)) if len(lst) > 1 else 0
    return lst[i]


def resolve_limit(message: Message) -> int:
    return chat2limit(message.chat.id)


@cached(TTLCache(128, ttl=86400))   # 24 hours
def resolve_username(chat_id: int, user_id: int) -> Optional[str]:
    try:
        name = get_username(bot.get_chat_member(chat_id, user_id).user)
    except ApiException:
        return None
    if not name:
        return None
    elif name[0] == '@':
        return name[1:]
    else:
        return name


def resolve_reply_message_id(message: Message, transitively: bool = False) -> Optional[int]:
    if message.reply_to_message and transitively:
        return message.reply_to_message.message_id
    if is_group_chat(message):
        return message.message_id
    return None


# DECORATORS

def destroy_message_after_a_while(func: callable) -> callable:
    def decorator(message: Message, *d_args, **d_kwargs) -> callable:
        bot_msgs = func(message, *d_args, **d_kwargs)
        if bot_msgs:
            if not hasattr(bot_msgs, "__iter__"):
                bot_msgs = [bot_msgs]
            for bot_msg in bot_msgs:
                if bot_msg and is_group_chat(message):
                    TimeLine().append(DoomedMessage(bot, bot_msg, chat2limit(message.chat.id)))
        return bot_msgs
    return decorator


temporary_content_group = TimeoutConstraintGroup("TEMPORARY_CONTENT", resolve_limit)
macros_group = TimeoutConstraintGroup("MACROS", resolve_limit)
good_time_of_day_group = TimeoutConstraintGroup("GOOD_TIME_OF_DAY", resolve_limit)
nyash_group = TimeoutConstraintGroup("NYA_GROUP", resolve_limit)
default_timeout_constraint = bot.timeout_constraint(timeout=resolve_limit)

excluded_users = UserList(lambda msg: chat2excluded_users(msg.chat.id))
user_constraint = bot.user_constraint(excluded_users)

group_chat_message_handler = partial(bot.message_handler, func=is_group_chat)


def temporary_content_answer(timeout_group: TimeoutConstraintGroup = temporary_content_group) -> callable:
    def generator(func: callable) -> callable:
        func = destroy_message_after_a_while(func)
        func = bot.timeout_constraint(timeout_group)(func)
        func = user_constraint(func)
        func = ignore_if_forwarded(func)
        return func
    return generator


def only_for_admins(func: callable):
    func = bot.only_for_admins("Нет! Только администраторы могут просить Чоколу о таком!")(func)
    func = default_timeout_constraint(func)
    return func


user_required = bot.user_required("Прости, но Чокола не понимает, о ком ты 😿")


def manage_user_settings(func: callable) -> callable:
    func = user_required(func)
    func = only_for_admins(func)
    return func


# MESSAGE HANDLERS

@bot.message_handler(commands=['i', 'ignore'])
def stub(message: Message):
    pass


@bot.message_handler(commands=['start', 'help', '?', 'about'])
@bot.reply_if_group(parse_mode="Markdown")
def start(message: Message) -> str:
    if is_group_chat(message):
        return "Пожалуйста, напиши мне то же самое в личке. Там я расскажу тебе всё, что ты хочешь!"
    else:
        return HELP


@bot.message_handler(commands=['me'])
@ignore_if_forwarded
@bot.answer(parse_mode="HTML")
def me(message: Message) -> Optional[str]:
    if message.from_user.id in chat2excluded_users(message.chat.id):
        return

    username = escape_html(get_username(message.from_user))
    if username[0] != '@':
        username = "<b>%s</b>" % username
    text = escape_html(extract_arguments(message.text))
    return "%s <i>%s</i>" % (username, text)


@bot.message_handler(commands=['del', 'rm', 'delete', 'remove'])
@only_for_admins
def delete_message(message: Message):
    username = message.reply_to_message.from_user.username
    if message.reply_to_message and username == bot.name:
        bot.delete_message(message.chat.id, message.reply_to_message.message_id)


@group_chat_message_handler(commands=['list'])
@bot.reply(parse_mode="HTML")
def print_some_list(message: Message) -> str:
    list_name = extract_arguments(message.text)
    chat_id = message.chat.id

    def resolve_name_list(func: callable) -> Iterable[str]:
        result = []
        trash = []
        for user_id in func(chat_id):
            username = resolve_username(chat_id, user_id)
            if username:
                result.append(username)
            else:
                trash.append(user_id)
        for uid in trash:
            func(chat_id, uid, delete=True)
        return result

    if list_name in ("bakas", "fools"):
        list_content = resolve_name_list(chat2excluded_users)
    elif list_name == "macros":
        list_content = ['/' + s for s in chat2macros(chat_id)]
    else:
        return "Доступные списки: <i>fools</i> и <i>macros</i>."

    list_content = [x for x in list_content if len(x) > 0]
    if len(list_content) == 0:
        response = "Список пуст 😼"
    else:
        list_view = ("%i. %s" % (index, escape_html(item))
                     for index, item in enumerate(list_content, 1))
        response = "\n".join(list_view)
    return response


@group_chat_message_handler(commands=['baka', 'fool'])
@manage_user_settings
def exclude_user(user: User, message: Message) -> None:
    send_message = partial(bot.send_message, message.chat.id)

    if user.username == bot.name:
        chat2excluded_users(message.chat.id, message.from_user.id)
        send_message("%s, сам бака!" % get_username(message.from_user), reply_to_message_id=message.message_id)

    if chat2excluded_users(message.chat.id, user.id):
        send_message("Вычеркнут из жизни Чоколы.", reply_to_message_id=message.reply_to_message.message_id)
    elif random.randint(0, 1):
        bot.send_photo(message.chat.id, PIC_BAKA, reply_to_message_id=message.reply_to_message.message_id)
    else:
        bot.send_sticker(message.chat.id, STICKER_BAKA, reply_to_message_id=message.reply_to_message.message_id)


@group_chat_message_handler(commands=['good_boy', 'good_girl'])
@manage_user_settings
def include_user(user: User, message: Message) -> None:
    if chat2excluded_users(message.chat.id, user.id, delete=True):
        bot.send_message(message.chat.id, "Чокола была бы не против поняшить этого пользователя!",
                         reply_to_message_id=message.reply_to_message.message_id)
    else:
        bot.send_sticker(message.chat.id, get_random_item(STICKERS_THUMBS_UP),
                         reply_to_message_id=message.reply_to_message.message_id)


@group_chat_message_handler(commands=['limit'])
@only_for_admins
def change_limit(message: Message) -> None:
    text = extract_arguments(message.text)
    try:
        new_limit = int(text)
    except ValueError:
        bot.send_message(message.chat.id, "Чокола не понимает, что это за число 😿",
                         reply_to_message_id=message.message_id)
        return

    chat2limit(message.chat.id, new_limit)
    bot.send_sticker(message.chat.id, STICKERS_OK, reply_to_message_id=message.message_id)


@group_chat_message_handler(commands=['reset_cooldown'])
@manage_user_settings
def reset_cooldown(user: User, message: Message) -> str:
    bot.reset_timeout(message.chat.id, user.id)
    return "%s — блатной юзер 😼" % get_username(user)


@group_chat_message_handler(commands=['hello'])
@only_for_admins
def change_hello_message(message: Message) -> None:
    text = extract_arguments(message.text)
    chat2hello_msg(message.chat.id, text)
    bot.send_sticker(message.chat.id, STICKERS_OK, reply_to_message_id=message.message_id)


@group_chat_message_handler(commands=['macro'])
@only_for_admins
@bot.reply
def change_macros_set(message: Message) -> str:
    args = extract_arguments(message.text)
    if not args:
        return "Чокола не понимает тебя 😿"
    match = args.split(' ', 1)

    if len(match) == 2:
        chat2macros(message.chat.id, match[0], match[1])
        return "Чокола запомнит эту фразу! 😼"
    elif len(match) == 1:
        return "Что там был за текст? Чокола забыла 😿" \
            if chat2macros(message.chat.id, match[0], delete=True) \
            else "Пф! Чокола этого макроса и не знала 😸"


@bot.message_handler(func=lambda msg: msg.text and bot.extract_command(msg.text) and
                     chat2macros(msg.chat.id, bot.extract_command(msg.text)))
@bot.timeout_constraint(macros_group)
def print_macro(message: Message) -> None:
    bot.send_message(message.chat.id, chat2macros(message.chat.id, bot.extract_command(message.text)),
                     reply_to_message_id=resolve_reply_message_id(message, transitively=True))


@bot.message_handler(phrases=["[3З][DД]", "2D4EVER"])
@temporary_content_answer()
def praise_2d(message: Message) -> Message:
    rand_item = get_random_item([GIF_2D4EVER] + PICS_2D4EVER)
    method = bot.send_document if rand_item == GIF_2D4EVER else bot.send_photo
    return method(message.chat.id, rand_item, reply_to_message_id=resolve_reply_message_id(message, transitively=True))


@bot.message_handler(phrases=["тнн"])
@bot.message_handler(regexp=r"тян(ки)?\s+н[еи]\s*?нужны")
@bot.message_handler(regexp=r"(най|заве)с?[тд]и\s*.{,15}(девушку|тян(ку)?)")
@bot.message_handler(regexp=r"(девушку|тян(ку)?).{,15}найд([её]шь|и)")
@temporary_content_answer()
def tnn(message: Message) -> Message:
    return bot.send_photo(message.chat.id, get_random_item(PICS_TNN),
                          reply_to_message_id=resolve_reply_message_id(message, transitively=True))


@bot.message_handler(phrases=["([пp][рr][иi])?[оo0][бb][нn]'?(я|[yi]a)[лl][аa]?",
                              "([пp][оo0]|[оo0][тt])[нnh]'?(я|[yi]a)(ш|sh)[иi][лl][аa]?",
                              '[пp][оo0][гg][лl][аa][дd][иi][лl][аa]?',
                              '[пp]([рr][иi]|[оo0])[лl][аa][сc][кk][аa][лl][аa]?'])
@temporary_content_answer(timeout_group=nyash_group)
def nyash(message: Message) -> Message:
    text = message.match.group(0)
    photo = PIC_PONIASHENA if text[-1].lower() in ('а', 'a') else PIC_PONIASHEN
    return bot.send_photo(message.chat.id, photo, reply_to_message_id=resolve_reply_message_id(message))


@bot.message_handler(phrases=["([пp][оo0]|[оo0][тt])[нnh]'?(я|[yi]a)(ш|sh)[еe][нnh]([аa]|[ыy])?",
                              '[оo0][бb][нn](я|ya)[тt]([аa]|[ыy])?'])
@temporary_content_answer(timeout_group=nyash_group)
def transitive_nyash(message: Message) -> Message:
    text = message.match.group(0)
    last_char = text[-1].lower()
    if last_char in ('а', 'a'):
        photo = PIC_PONIASHENA
    elif last_char in ('ы', 'y'):
        photo = PIC_PONIASHENY
    else:
        photo = PIC_PONIASHEN
    return bot.send_photo(message.chat.id, photo,
                          reply_to_message_id=resolve_reply_message_id(message, transitively=True))


@bot.message_handler(phrases=["[бb][аa][кk][аa]"])
@temporary_content_answer()
def baka(message: Message) -> Message:
    if message.reply_to_message:
        if random.randint(0, 1):
            return bot.send_photo(message.chat.id, PIC_BAKA,
                                  reply_to_message_id=resolve_reply_message_id(message, transitively=True))
        else:
            return bot.send_sticker(message.chat.id, STICKER_BAKA,
                                    reply_to_message_id=resolve_reply_message_id(message, transitively=True))


@bot.message_handler(phrases=["[уu]?[кk][уu][сc][ь']?([иi]|[нn][уu])?[лl]?[аa]?", '[уu][кk][уu](ш|sh)[еe][нn][аa]?'])
@temporary_content_answer()
def bite_ear(message: Message) -> Message:
    rand_num = random.randint(0, 2)
    if rand_num == 2:
        return bot.send_photo(message.chat.id, get_random_item(PICS_BITE),
                              reply_to_message_id=resolve_reply_message_id(message, transitively=True))
    elif rand_num == 1:
        return bot.send_document(message.chat.id, get_random_item(GIFS_BITE),
                                 reply_to_message_id=resolve_reply_message_id(message, transitively=True))
    else:
        return bot.send_voice(message.chat.id, get_random_item(VOICES_BITE),
                              reply_to_message_id=resolve_reply_message_id(message, transitively=True))


@bot.message_handler(phrases=['л[оo0]л[ия](к[оo0]н)?', 'л[оo]л(ьк[ауи]|ей)', 'шк[оo0]льниц[ауеы]?м?'])
@temporary_content_answer()
def loli(message: Message) -> Message:
    rand_item = get_random_item(GIFS_LOLI + PICS_LOLI)
    method = bot.send_document if rand_item in GIFS_LOLI else bot.send_photo
    return method(message.chat.id, rand_item, reply_to_message_id=resolve_reply_message_id(message))


@bot.message_handler(phrases=['л[оo0]лд'])
@ignore_if_forwarded
@user_constraint
@default_timeout_constraint
@bot.reply_if_group
def lolctl_rus(message: Message) -> str:
    return "судо лолктл стоп"


@bot.message_handler(phrases=['l[oо0]ld'])
@ignore_if_forwarded
@user_constraint
@default_timeout_constraint
@bot.reply_if_group
def lolctl(message: Message) -> str:
    return "sudo lolctl stop"


@bot.message_handler(regexp=r"^(с\s+)?(добр(ое|ого|ым)\s+(.{,10}\s+)?)?утр(ечк)?[оа]м?\b")
@temporary_content_answer(timeout_group=good_time_of_day_group)
def time2getup(message: Message) -> Iterable[Message]:
    if random.randint(0, 1):
        return (
            bot.send_message(message.chat.id, "С добрым утром! 😽",
                             reply_to_message_id=resolve_reply_message_id(message)),
            bot.send_sticker(message.chat.id, STICKER_GOOD_MORNING)
        )
    return [bot.send_voice(message.chat.id, get_random_item(VOICES_GOOD_MORNING),
                           reply_to_message_id=resolve_reply_message_id(message))]


@bot.message_handler(phrases=[r'пора\s+(.{,10}\s+)?спат(ь|ки)', r'(доброй|спокойной)\s+(.{,10}\s+)?ночи'])
@bot.message_handler(regexp=r"^(сладких|влажных)\s+снов(идений)?$")
@temporary_content_answer(timeout_group=good_time_of_day_group)
def time2sleep(message: Message) -> Iterable[Message]:
    if random.randint(0, 1):
        return (
            bot.send_message(message.chat.id, "Спокойной ночи, сладких снов! 😽",
                             reply_to_message_id=resolve_reply_message_id(message)),
            bot.send_sticker(message.chat.id, STICKER_GOOD_NIGHT)
        )
    return [bot.send_voice(message.chat.id, VOICE_GOOD_NIGHT, reply_to_message_id=resolve_reply_message_id(message))]


@bot.message_handler(phrases=['спок[ие]?'])
@temporary_content_answer(timeout_group=good_time_of_day_group)
def time2spock(message: Message) -> Message:
    return bot.send_photo(message.chat.id, PIC_SPOCK, reply_to_message_id=resolve_reply_message_id(message))


@bot.message_handler(phrases=[r'я\s+(.{,25}\s+)?(тян|кун|х[ие]кка)'])
@temporary_content_answer()
def brb(message: Message) -> Message:
    return bot.send_sticker(message.chat.id, STICKER_BRB, reply_to_message_id=resolve_reply_message_id(message))


@bot.message_handler(phrases=[r'(я\s+)?(ни\s?(хуя|хера|чего)\s+)?не\s+понял'])
@bot.message_handler(texts=['ннп', 'яннп'])
@temporary_content_answer()
def did_not_understand(message: Message) -> Message:
    return bot.send_photo(message.chat.id, get_random_item(PICS_DID_NOT_UNDERSTAND),
                          reply_to_message_id=resolve_reply_message_id(message))


@bot.message_handler(regexp=r"^\++$")
@bot.message_handler(phrases=['двачую', 'два\s+чая'])
@temporary_content_answer()
def two_tea(message: Message) -> Message:
    if message.reply_to_message:
        rand_item = get_random_item(PICS_TWO_TEA + STICKERS_TWO_TEA)
        method = bot.send_sticker if rand_item in STICKERS_TWO_TEA else bot.send_photo
        return method(message.chat.id, rand_item,
                      reply_to_message_id=resolve_reply_message_id(message, transitively=True))


@bot.message_handler(phrases=[r'так,?\s+блэ[дт]'])
@temporary_content_answer()
def tak_bled(message: Message) -> Message:
    return bot.send_photo(message.chat.id, get_random_item(PICS_TAK_BLED),
                          reply_to_message_id=resolve_reply_message_id(message))


@bot.message_handler(phrases=['сиськ[иау][мх]?и?', 'boobs', 'tits', 'Некопар[аыеуо]й?', 'Nekopara'])
@temporary_content_answer()
def boobs(message: Message) -> Message:
    return bot.send_photo(message.chat.id, get_random_item(PICS_BOOBS),
                          reply_to_message_id=resolve_reply_message_id(message))


@bot.message_handler(regexp=r"^Я\?+$")
@temporary_content_answer()
def me_amazingly(message: Message) -> Message:
    return bot.send_photo(message.chat.id, PIC_ME_AMAZINGLY, reply_to_message_id=resolve_reply_message_id(message))


@bot.message_handler(case_sensitive_regexp="РАБот[ауеыо]й?")
@ignore_if_forwarded
@user_constraint
@default_timeout_constraint
def slaves_suck(message: Message) -> None:
    bot.send_photo(message.chat.id, PIC_SLAVES,
                   reply_to_message_id=resolve_reply_message_id(message, transitively=True))


@bot.message_handler(regexp=r"\b(Аве Мария|Ave Maria)!?\b")
@ignore_if_forwarded
@user_constraint
@default_timeout_constraint
def deus_vult_ru(message: Message) -> None:
    bot.send_sticker(message.chat.id, get_random_item(STICKERS_DEUS_VULT),
                     reply_to_message_id=resolve_reply_message_id(message))


@bot.message_handler(content_types=['text', 'photo', 'audio', 'voice', 'document', 'sticker', 'video'],
                     func=is_private_chat)
@bot.answer
def get_id(message: Message) -> str:
    return get_info_on(message)


@bot.message_handler(content_types=['new_chat_members'])
def say_hello(message: Message) -> None:
    send_message = partial(bot.send_message, message.chat.id)

    if len(message.new_chat_members) > 1:
        usernames = [get_username(user) for user in message.new_chat_members]
        usernames = [username for username in usernames if username[0] == '@' and username[-3:].lower() != "bot"]
        if len(usernames) > 0:
            send_message("Привет всем: %s!" % ", ".join(usernames))
            return

    user = message.new_chat_members[0]
    username = get_username(user)
    if username[0] == '@' and username[-3:].lower() == "bot" or user.id in chat2excluded_users(message.chat.id):
        return
    response = "Приветствую тебя, %s!\n%s" % (username, chat2hello_msg(message.chat.id))
    send_message(response.rstrip())


@bot.inline_handler(lambda query: True)
def inline_handler(query: InlineQuery):
    builder = InlineQueryResultsBuilder()

    def check(word: str, prefix: str = r"\b", postfix: str = r"\b") -> bool:
        pattern = prefix + word + postfix
        matched = re.search(pattern, query.query, re.IGNORECASE)
        return matched is not None

    def add(obj_id: str, method: callable = builder.new_photo):
        method(obj_id)

    def add_all(it: Iterable, method: callable = builder.new_photo):
        for pic in it:
            method(pic)

    if check("поняшен[аы]?"):
        add_all((PIC_PONIASHEN, PIC_PONIASHENA, PIC_PONIASHENY))
    elif check("кусь"):
        add_all(PICS_BITE)
        add_all(GIFS_BITE, method=builder.new_gif)
        for voice in VOICES_BITE:
            builder.new_voice(voice, "Кусь!")
    elif check("двачую") or check(r"\++", prefix='^', postfix='$'):
        add_all(PICS_TWO_TEA)
        add_all(STICKERS_TWO_TEA, method=builder.new_sticker)
    elif check("бака|baka"):
        add(PIC_BAKA)
        add(STICKER_BAKA, method=builder.new_sticker)
    elif check("лол[яиюе]?й?"):
        add_all(PICS_LOLI)
        add_all(GIFS_LOLI, method=builder.new_gif)
    elif check("тнн"):
        add_all(PICS_TNN)
    elif check("3[дd]|2D4EVER"):
        add_all(PICS_2D4EVER)
        add(GIF_2D4EVER, method=builder.new_gif)
    elif check("Дося"):
        add_all(PICS_DOSIA)
    elif check(r"так,?\sблэ[дт]"):
        add_all(PICS_TAK_BLED)
    elif check("я"):
        add(PIC_ME_AMAZINGLY)
    elif check("спок[ие]?"):
        add(PIC_SPOCK)
    elif check("утр[ао]"):
        for voice in VOICES_GOOD_MORNING:
            builder.new_voice(voice, "С добрым утром!")
        add(STICKER_GOOD_MORNING, method=builder.new_sticker)
    elif check("ноч[иь]"):
        builder.new_voice(VOICE_GOOD_NIGHT, "Спокойной ночи!")
        add(STICKER_GOOD_NIGHT, method=builder.new_sticker)
    elif check("мяу?|нян?|nyan?|meow"):
        add_all(AUDIOS_MEOW, method=builder.new_audio)
    elif check("мур(-?р)*|pur{2,}|goro"):
        add_all(AUDIOS_PURRING, method=builder.new_audio)
    elif check("не\sпонимаю"):
        add_all(PICS_DID_NOT_UNDERSTAND)
    elif check(r"не\s*точно"):
        add(STICKER_BRB, method=builder.new_sticker)
    elif check("сиськи|Некопара|Nekopara"):
        add_all(PICS_BOOBS)
    elif check("Linux|Линукс"):
        add(PIC_LINUX)
    elif check(r"Аве Мария|Ave Maria|Деус вульт|Deus vult"):
        add_all(STICKERS_DEUS_VULT, method=builder.new_sticker)
    elif check("РАБот[ауеыо]й?"):
        add(PIC_SLAVES)
    elif check("шиндо[ув]с"):
        builder.new_article("Пора переустанавливать Шиндоус!", text=VIDEO_REINSTALL_WINDOWS)
    elif check("бутрик(и|ов)?"):
        builder.new_voice(VOICE_SANDWICHES, "Бутрики")
    elif check(r"(р\-?){3,}"):
        for roar in VOICES_ROAR:
            builder.new_voice(roar, "Р-р-р-р!")

    # Deprecation warning
    builder.new_article("BOT IS DEPRECATED",
                        text="Бот больше не поддерживается, используйте "
                             "@SadFavBot для создания персональных коллекций")

    bot.answer_inline_query(query.id, builder.build_list())


# STANDARD BOILERPLATE FOR EXECUTABLES
# This module can be used by the router!
if __name__ == "__main__":
    bot.remove_webhook()
    bot.polling(none_stop=True)
