"""A module that provides detailed service information about messages in a text form."""


def get_info_on(message) -> str:
    info = _MessageInfo(message)
    return "\n\n".join((x for x in (
        info.parse_message(),
        info.parse_attachment(),
        info.parse_forwarded()
    ) if x != ""))


class _MessageInfo:
    def __init__(self, message):
        self._message = message

    def parse_message(self) -> str:
        return "Author ID: %i\nHis language: %s\nMessage ID: %i\nChat ID: %i" % (
            self._message.from_user.id,
            self._message.from_user.language_code,
            self._message.message_id,
            self._message.chat.id
        )

    def parse_attachment(self) -> str:
        return str(_Attachment.get_attachment(self._message))

    def parse_forwarded(self) -> str:
        return str(_ForwardedInfo.get_info(self._message))


class _Nullable:
    def __str__(self):
        return ""


class _Attachment(_Nullable):
    def __init__(self, name, file_id):
        self._name = name
        self._file_id = file_id

    @staticmethod
    def get_attachment(message) -> _Nullable:
        if message.photo:
            return _PhotoList(message.photo)
        elif message.sticker:
            return _Attachment("Sticker", message.sticker.file_id)
        elif message.document:
            return _Attachment("Document", message.document.file_id)
        elif message.audio:
            return _Attachment("Audio", message.audio.file_id)
        elif message.voice:
            return _Attachment("Voice", message.voice.file_id)
        elif message.video:
            return _Attachment("Video", message.video.file_id)
        return _Nullable()

    def __str__(self) -> str:
        return "%s ID: %s" % (self._name, self._file_id)


class _PhotoList(_Nullable):
    def __init__(self, photo_list):
        self._list = photo_list

    def __str__(self):
        return "\n".join(("Photo (%i, %i) ID: %s" % (p.width, p.height, p.file_id) for p in self._list))


class _ForwardedInfo(_Nullable):
    def __init__(self):
        self.author = None
        self.origin_chat = None
        self.origin_id = 0

    @staticmethod
    def get_info(message) -> _Nullable:
        if message.forward_from or message.forward_from_chat:
            info = _ForwardedInfo()
            if message.forward_from:
                info.author = message.forward_from
            if message.forward_from_chat:
                info.origin_chat = message.forward_from_chat
                info.origin_id = message.forward_from_message_id
            return info
        else:
            return _Nullable()

    def __str__(self):
        def safe_str(template, var):
            if not var:
                return ""

            if hasattr(var, "id"):
                var = var.id
            return template % var

        str_repr = safe_str("Author ID: %i", self.author) + \
                   safe_str("\nOriginal chat ID: %i", self.origin_chat) + \
                   safe_str("\nOriginal message ID: %i", self.origin_id)
        return ("Forwarded message:\n" + str_repr) if str_repr else ""
