"""A module that's responsible for settings storage for all chats."""

import os
import pickle
import inspect

from typing import Sequence, MutableSequence, Sized
from enum import Enum

from pymonad.Maybe import *
from pymonad import curry

from botutils.helpers import get_safe_get_closure
from kozalo_bot.settings import STORAGE


# CONST

DEFAULT_DELAY_PER_USER = 600  # in seconds


# INTERNAL COLLECTIONS

__chat2limit = {}
__chat2excluded_users = {}

__chat2hello_msg = {}
__chat2macros = {}


# MAIN SAVE/LOAD FUNCTIONS
# to encapsulate access to the disk

def save_state():
    frame = inspect.getouterframes(inspect.currentframe())[1]
    module_path = os.path.dirname(frame.filename)
    path = os.path.join(module_path, STORAGE)

    with open(path, 'wb') as f:
        pickle.dump({
            'chat2limit': __chat2limit,
            'chat2excludedUsers': __chat2excluded_users,
            'chat2hello_msg': __chat2hello_msg,
            'chat2macros': __chat2macros
        }, f)


def restore_state():
    frame = inspect.getouterframes(inspect.currentframe())[1]
    module_path = os.path.dirname(frame.filename)
    path = os.path.join(module_path, STORAGE)

    if not os.path.isfile(path):
        return

    with open(path, 'rb') as f:
        global __chat2limit, __chat2excluded_users, __chat2hello_msg, __chat2macros
        data = pickle.load(f)
        get_or_default = get_safe_get_closure(data, {})

        __chat2limit = get_or_default("chat2limit")
        __chat2excluded_users = get_or_default("chat2excludedUsers")
        __chat2hello_msg = get_or_default("chat2hello_msg")
        __chat2macros = get_or_default("chat2macros")


# UTILITIES

class Feature(Enum):
    STICKER_DUPLICATION_ELIMINATION = "sde"


# ...FOR FUNCTIONAL PROGRAMMING

@curry
def get(key, d: dict) -> Maybe:
    return Just(d[key]) if key in d else Nothing


@curry
def delete_from_seq(elem, l: MutableSequence) -> Maybe:
    if elem in l:
        l.remove(elem)
        return Just(l)
    else:
        return Nothing


@curry
def delete_from_dict(key, d: dict) -> Maybe:
    if key in d:
        del d[key]
        return Just(d)
    else:
        return Nothing


@curry
def has(key, container: Sequence) -> Maybe:
    return Just(key in container)


def is_empty(container: Sized) -> Maybe:
    return Just(len(container) == 0)


# FUNCTIONS FOR PUBLIC ACCESS TO THE DATA

def chat2limit(chat_id: int, limit: int = None) -> int:
    if chat_id not in __chat2limit:
        __chat2limit[chat_id] = DEFAULT_DELAY_PER_USER
    if limit is not None:
        __chat2limit[chat_id] = limit
        save_state()
    return __chat2limit[chat_id]


def chat2excluded_users(chat_id: int, user_id: int = None, delete: bool = False):
    chat_excluded_users = Just(__chat2excluded_users) >> get(chat_id)
    if user_id is not None:
        if delete:
            was_deleted = chat_excluded_users >> delete_from_seq(user_id)
            if was_deleted is not Nothing:
                if (chat_excluded_users >> is_empty).value:
                    del __chat2excluded_users[chat_id]
                save_state()
                return True
            else:
                return False
        else:
            if chat_id not in __chat2excluded_users:
                __chat2excluded_users[chat_id] = []
            if user_id not in __chat2excluded_users[chat_id]:
                __chat2excluded_users[chat_id].append(user_id)
                save_state()
                return True
            else:
                return False
    else:
        return chat_excluded_users.value or []


def chat2hello_msg(chat_id: int, text: str = None) -> str:
    if text is not None:
        if len(text) == 0:
            if chat_id in __chat2hello_msg:
                del __chat2hello_msg[chat_id]
                save_state()
        else:
            __chat2hello_msg[chat_id] = text
            save_state()
    return __chat2hello_msg[chat_id] if chat_id in __chat2hello_msg else ""


def chat2macros(chat_id: int, trigger: str = None, text: str = None, delete: bool = False):
    chat_macros = Just(__chat2macros) >> get(chat_id)
    if not trigger:
        return chat_macros.value or {}
    if delete:
        was_deleted = chat_macros >> delete_from_dict(trigger)
        if was_deleted is not Nothing:
            if (chat_macros >> is_empty).value:
                del __chat2macros[chat_id]
            save_state()
            return True
        else:
            return False
    if text:
        if chat_id not in __chat2macros:
            __chat2macros[chat_id] = {}
        __chat2macros[chat_id][trigger] = text
        save_state()
    return (chat_macros >> get(trigger)).value or ""
